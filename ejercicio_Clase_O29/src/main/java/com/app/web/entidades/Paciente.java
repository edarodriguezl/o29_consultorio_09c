package com.app.web.entidades;

import java.sql.Date;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

@Entity
@Table(name="Paciente")
public class Paciente {
	
	// ATRIBUTOS
	//ATRIBUTOS CLASE
	
	
	@Id
	@GeneratedValue
	private int cedulaPaciente;
	
	@Column(name="nombrePaciente", nullable=false, length=50)
	private String nombrePaciente; 
	
	@Column(name="apellidoPaciente", nullable=false, length=50)
	private String apellidoPaciente; 
	
	@Column(name = "fechaNacimiento", length = 20, nullable = false)
	private Date fechaNacimiento;
	
	@Column(name = "edadPaciente", length = 20, nullable = true)
	private int edadPaciente;
	
	@Column(name="direccionPaciente", nullable=false, length=20)
	private String direccionPaciente;
	
	@OneToMany(mappedBy="ceduPaciente")
	private medicamento med;
	
	//CONSUTRUCTORES 
	
	
	public Paciente() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Paciente(int cedulaPaciente, String nombrePaciente, String apellidoPaciente, Date fechaNacimiento,
			int edadPaciente, String direccionPaciente, medicamento med) {
		super();
		this.cedulaPaciente = cedulaPaciente;
		this.nombrePaciente = nombrePaciente;
		this.apellidoPaciente = apellidoPaciente;
		this.fechaNacimiento = fechaNacimiento;
		this.edadPaciente = edadPaciente;
		this.direccionPaciente = direccionPaciente;
		this.med = med;
	}


	//GETTER AND SETTER - ENCAPSULAMIENTO

	public int getCedulaPaciente() {
		return cedulaPaciente;
	}

	

	public void setCedulaPaciente(int cedulaPaciente) {
		this.cedulaPaciente = cedulaPaciente;
	}

	public String getNombrePaciente() {
		return nombrePaciente;
	}

	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}

	public String getApellidoPaciente() {
		return apellidoPaciente;
	}

	public void setApellidoPaciente(String apellidoPaciente) {
		this.apellidoPaciente = apellidoPaciente;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public int getEdadPaciente() {
		return edadPaciente;
	}

	public void setEdadPaciente(int edadPaciente) {
		this.edadPaciente = edadPaciente;
	}

	public String getDireccionPaciente() {
		return direccionPaciente;
	}

	public void setDireccionPaciente(String direccionPaciente) {
		this.direccionPaciente = direccionPaciente;
	}

	
	public medicamento getMed() {
		return med;
	}


	public void setMed(medicamento med) {
		this.med = med;
	}

	
	
	//SOBREESCRIBIR EL METODO TOSTRING
	
	@Override
	public String toString() {
		return "Paciente [cedulaPaciente=" + cedulaPaciente + ", nombrePaciente=" + nombrePaciente
				+ ", apellidoPaciente=" + apellidoPaciente + ", fechaNacimiento=" + fechaNacimiento + ", edadPaciente="
				+ edadPaciente + ", direccionPaciente=" + direccionPaciente + ", med=" + med + "]";
	}
	
	
	




}
