package com.app.web.entidades;

import java.sql.Date;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;


@Entity
@Table(name="medico")
public class medico {
	
	//ATRIBUTOS CLASE
	
	@Id
	@GeneratedValue
	private int cedulaMedico;
	
	@Column(name="nombreMedico", nullable=false, length=50)
	private String nombreMedico; 
	
	@Column(name="apellidoMedico", nullable=false, length=50)
	private String apellidoMedico; 
	
	@Column(name = "especialidadMedico", length = 30, nullable = false)
	private String especialidadMedico;
	
	@Column(name="correoMedico", nullable=false, length=30, unique=true)
	private String correoMedico;

	
	
	//CONSUTRUCTORES 
	
	public medico() {
		super();
		// TODO Auto-generated constructor stub
	}



	public medico(int cedulaMedico, String nombreMedico, String apellidoMedico, String especialidadMedico,
			String correoMedico) {
		super();
		this.cedulaMedico = cedulaMedico;
		this.nombreMedico = nombreMedico;
		this.apellidoMedico = apellidoMedico;
		this.especialidadMedico = especialidadMedico;
		this.correoMedico = correoMedico;
	}
	
	
	//GETTER AND SETTER - ENCAPSULAMIENTO


	public int getCedulaMedico() {
		return cedulaMedico;
	}



	public void setCedulaMedico(int cedulaMedico) {
		this.cedulaMedico = cedulaMedico;
	}



	public String getNombreMedico() {
		return nombreMedico;
	}



	public void setNombreMedico(String nombreMedico) {
		this.nombreMedico = nombreMedico;
	}



	public String getApellidoMedico() {
		return apellidoMedico;
	}



	public void setApellidoMedico(String apellidoMedico) {
		this.apellidoMedico = apellidoMedico;
	}



	public String getEspecialidadMedico() {
		return especialidadMedico;
	}



	public void setEspecialidadMedico(String especialidadMedico) {
		this.especialidadMedico = especialidadMedico;
	}



	public String getCorreoMedico() {
		return correoMedico;
	}



	public void setCorreoMedico(String correoMedico) {
		this.correoMedico = correoMedico;
	}
	
	//SOBREESCRIBIR EL METODO TOSTRING 

	@Override
	public String toString() {
		return "medico [cedulaMedico=" + cedulaMedico + ", nombreMedico=" + nombreMedico + ", apellidoMedico="
				+ apellidoMedico + ", especialidadMedico=" + especialidadMedico + ", correoMedico=" + correoMedico
				+ "]";
	}
	
	
	

}
