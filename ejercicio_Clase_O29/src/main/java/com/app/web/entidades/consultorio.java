package com.app.web.entidades;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;


@Entity
@Table(name="consultorio")

public class consultorio {
	
	//ATRIBUTOS CLASE
	
	@Id
	@GeneratedValue
	private int codigoConsultorio;
	
	@Column(name="direccionConsultorio", nullable=false, length=50)
	private String direccionConsultorio;
	
	@Column(name="pisoConsultorio", nullable=false, length=20)
	private int pisoConsultorio;

	//CONSUTRUCTORES 
	
	public consultorio() {
		super();
		// TODO Auto-generated constructor stub
	}

	public consultorio(int codigoConsultorio, String direccionConsultorio, int pisoConsultorio) {
		super();
		this.codigoConsultorio = codigoConsultorio;
		this.direccionConsultorio = direccionConsultorio;
		this.pisoConsultorio = pisoConsultorio;
	}
	
	//GETTER AND SETTER - ENCAPSULAMIENTO

	public int getCodigoConsultorio() {
		return codigoConsultorio;
	}

	public void setCodigoConsultorio(int codigoConsultorio) {
		this.codigoConsultorio = codigoConsultorio;
	}

	public String getDireccionConsultorio() {
		return direccionConsultorio;
	}

	public void setDireccionConsultorio(String direccionConsultorio) {
		this.direccionConsultorio = direccionConsultorio;
	}

	public int getPisoConsultorio() {
		return pisoConsultorio;
	}

	public void setPisoConsultorio(int pisoConsultorio) {
		this.pisoConsultorio = pisoConsultorio;
	}

	//SOBREESCRIBIR EL METODO TOSTRING 
	
	@Override
	public String toString() {
		return "consultorio [codigoConsultorio=" + codigoConsultorio + ", direccionConsultorio=" + direccionConsultorio
				+ ", pisoConsultorio=" + pisoConsultorio + "]";
	}
	

}
