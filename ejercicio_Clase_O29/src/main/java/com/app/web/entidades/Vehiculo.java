package com.app.web.entidades;

import javax.persistence.*;

@Entity
@Table(name="vehiculos")
public class Vehiculo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idVehiculo; 
	@Column(name ="placaVehiculo", nullable = false, length = 6)
	private String placaVehiculo; 
	@Column(name ="marcaVehiculo", nullable = true, length = 30)
	private String marca;
	@Column(name ="tipoVehiculo", nullable = false, length = 30)
	private String tipo;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn(name="idClienteFK", referencedColumnName="idCliente")
	public Cliente vehiculoCliente;

	

	//CONSTRUCTOR 
	public Vehiculo() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Vehiculo(int idVehiculo, String placaVehiculo, String marca, String tipo, Cliente vehiculoCliente) {
		super();
		this.idVehiculo = idVehiculo;
		this.placaVehiculo = placaVehiculo;
		this.marca = marca;
		this.tipo = tipo;
		this.vehiculoCliente = vehiculoCliente;
	}

	//GETTER AND SETTER

	public int getIdVehiculo() {
		return idVehiculo;
	}



	public void setIdVehiculo(int idVehiculo) {
		this.idVehiculo = idVehiculo;
	}



	public String getPlacaVehiculo() {
		return placaVehiculo;
	}



	public void setPlacaVehiculo(String placaVehiculo) {
		this.placaVehiculo = placaVehiculo;
	}



	public String getMarca() {
		return marca;
	}



	public void setMarca(String marca) {
		this.marca = marca;
	}



	public String getTipo() {
		return tipo;
	}



	public void setTipo(String tipo) {
		this.tipo = tipo;
	}



	public Cliente getVehiculoCliente() {
		return vehiculoCliente;
	}



	public void setVehiculoCliente(Cliente vehiculoCliente) {
		this.vehiculoCliente = vehiculoCliente;
	}


	//SOBRE ESCRIBIR EL METODO TO STRING
	@Override
	public String toString() {
		return "Vehiculo [idVehiculo=" + idVehiculo + ", placaVehiculo=" + placaVehiculo + ", marca=" + marca
				+ ", tipo=" + tipo + ", vehiculoCliente=" + vehiculoCliente + "]";
	}

}