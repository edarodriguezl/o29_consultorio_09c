package com.app.web.entidades;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;


@Entity
@Table(name="medicamento")

public class medicamento {
	
	//ATRIBUTOS CLASE
	
	@Id
	@GeneratedValue
	private String nombreMedicamento;
	
	@Column(name="dosisMedicamento", nullable=false, length=20)
	private int dosisMedicamento;
	
	//LLAVE FORANEA
	@OneToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name="ceduPaciente", referencedColumnName="cedulaPaciente")
	private Paciente ceduPaciente;

	
	//CONSUTRUCTORES
	
	public medicamento() {
		super();
		// TODO Auto-generated constructor stub
	}


	public medicamento(String nombreMedicamento, int dosisMedicamento, Paciente ceduPaciente) {
		super();
		this.nombreMedicamento = nombreMedicamento;
		this.dosisMedicamento = dosisMedicamento;
		this.ceduPaciente = ceduPaciente;
	}

	//GETTER AND SETTER - ENCAPSULAMIENTO

	public String getNombreMedicamento() {
		return nombreMedicamento;
	}


	public void setNombreMedicamento(String nombreMedicamento) {
		this.nombreMedicamento = nombreMedicamento;
	}


	public int getDosisMedicamento() {
		return dosisMedicamento;
	}


	public void setDosisMedicamento(int dosisMedicamento) {
		this.dosisMedicamento = dosisMedicamento;
	}


	public Paciente getCeduPaciente() {
		return ceduPaciente;
	}


	public void setCeduPaciente(Paciente ceduPaciente) {
		this.ceduPaciente = ceduPaciente;
	}


	//SOBREESCRIBIR EL METODO TOSTRING
	
	@Override
	public String toString() {
		return "medicamento [nombreMedicamento=" + nombreMedicamento + ", dosisMedicamento=" + dosisMedicamento
				+ ", ceduPaciente=" + ceduPaciente + "]";
	} 
	
	
	
	
	
	

}
